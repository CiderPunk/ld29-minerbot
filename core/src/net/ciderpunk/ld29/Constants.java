package net.ciderpunk.ld29;

import com.badlogic.gdx.graphics.Color;

public class Constants {

	

	public static final Color opaque = new Color(1f,1f,1f,1f);
	public static final Color transparent = new Color(1f,1f,1f,0.7f);
	
	
	public static final Color clearColour = new Color(0.3f,0.2f,0.2f,0f);
	public static final Color skyColour = new Color(0.7f,0.7f,1f,1f);
	public static final String AtlasPath = "res.atlas";
	public static int TileSize = 32;
	public static final String SmallFont = "fonts/dpcomic.fnt";
	
	public static final String statusTemplate = "Need: %d, Perfect: %d\nCollected: %d";
	
	//ms before a tap becomes a press
	public static final int tapMaxTime = 300;
	//squared max pixels a drag can move before it is considered a drag and not a tap
	public static final float dragMaxDist = 30f;
	

	public static final String[] mapList = new String[]{ "maps/map2.json", "maps/map3.json", "maps/map4.json", "maps/map5.json","maps/map7.json","maps/map6.json" };
}
