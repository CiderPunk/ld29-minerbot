package net.ciderpunk.ld29.ents;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.ents.Order.OrderType;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class SpaceCaptain extends MineBotEnt {

	public static Animation animPoint;
	public static Animation animCry;

	@Override
	public void getTextures(TextureAtlas atlas) {
		animPoint = this.buildAnim(atlas, "cakes/point", 0.5f, 0, 0);
		animCry = this.buildAnim(atlas, "cakes/cry", 0.5f, 0, 0);
	}

	public SpaceCaptain() {
		super();
	}
	
	public SpaceCaptain(GuiElement owner, float x, float y) {
		super(owner, (int) x, (int)y);
		this.setAnim(animPoint);
		this.currentFrame = (Frame) this.currentAnim.getKeyFrame(0f);
	}

	@Override
	public Boolean update(float dT) {
		// TODO Auto-generated method stub
		return super.update(dT);
	}

	public void cry() {
		this.setAnim(animCry);
		
	}

	
	
}
