package net.ciderpunk.ld29.ents;

import java.util.Iterator;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.EntityList;

public class SimulationEntityList extends EntityList {
	public EntityList doTick(float nextTick) {
    Entity ent;
    mainBuffer.iter();
    while ((ent = mainBuffer.next()) != null) {
      if (ent instanceof SimulationEnt) {
        if (!((SimulationEnt) ent).tick(nextTick)) {
          mainBuffer.remove();
        }
      }
    }
    return this;
  }
	
	public MineBotEnt findOccupant(int x,int y){
    Entity ent;
    mainBuffer.iter();
    while ((ent = mainBuffer.next()) != null) {
      if (ent instanceof SimulationEnt) {
        if (((SimulationEnt) ent).getX() == x && ((SimulationEnt) ent).getY() == y){
          return (SimulationEnt) ent;
        }
      }
    }
    return null;
	}
	

}
