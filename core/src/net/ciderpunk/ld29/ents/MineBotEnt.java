package net.ciderpunk.ld29.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.AnimatedEntity;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;
import net.ciderpunk.ld29.map.Map;
import net.ciderpunk.ld29.tiles.Dummy;

public abstract class MineBotEnt extends AnimatedEntity implements IResourceUser {
	//current tile coordinates
	protected int x, y;
	
	public MineBotEnt() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MineBotEnt(GuiElement owner, int x, int y) {
		super(owner, x, y);
		this.x = x;
		this.y = y;
		getOwnerMap().setDummy(x,y);
	}

	public MineBotEnt(GuiElement owner, Vector2 loc) {
		super(owner, loc);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
		
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		getTextures(atlas);
		
	}
	
	public abstract void getTextures(TextureAtlas atlas);

	public void draw(SpriteBatch batch){
		currentFrame.draw(batch, loc.x * (float) Constants.TileSize, loc.y * (float) Constants.TileSize);
	}

	public Map getOwnerMap(){return (Map)this.getOwner(); }
	
}
	
