package net.ciderpunk.ld29.ents;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.tiles.Tile;
import net.ciderpunk.ld29.tiles.TileSet;

public class FallingGem extends FallingEnt {

	static Frame gemFrame;
	
	public FallingGem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FallingGem(GuiElement owner, int x, int y) {
		super(owner, x, y);
		currentFrame = gemFrame;
	}

	@Override
	public void getTextures(TextureAtlas atlas) {
		gemFrame = new Frame(atlas.findRegion("gem"), 0, 0);
	}
	
	@Override
	protected Tile getTile() {
		TileSet.tinkleSfx.play(MathUtils.random(0.4f, 0.6f));		
		return TileSet.getTile('g');
	}
	
	
}
