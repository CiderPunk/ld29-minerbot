package net.ciderpunk.ld29.ents;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;
import net.ciderpunk.ld29.ents.Order.OrderType;
import net.ciderpunk.ld29.map.Map;
import net.ciderpunk.ld29.tiles.GrassDropoff;
import net.ciderpunk.ld29.tiles.Tile;
import net.ciderpunk.ld29.tiles.TileSet;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class RoBro extends SimulationEnt {


	protected static Animation animleft;
	protected static Animation animRight;
	protected int gemCount;
	protected static BitmapFont font; 

	boolean alive;
	OrderType currentOrder ;

	public RoBro() {
		super();
		alive = true;
		// TODO Auto-generated constructor stub
	}

	public RoBro(GuiElement owner, float x, float y, OrderType initOrder) {
		super(owner, (int) x, (int)y);
		this.setAnim(getOrderAnim(initOrder));
		this.currentFrame = (Frame) this.currentAnim.getKeyFrame(0f);
		currentOrder = initOrder;
		gemCount = 0;
		alive = true;
	}


	@Override
	public void getTextures(TextureAtlas atlas) {
		animleft = this.buildAnim(atlas, "robro_left", 0.1f, 0,-1);
		animRight = this.buildAnim(atlas, "robro_right", 0.1f, 0,-1);
	}


	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.SmallFont, BitmapFont.class);
		

		super.preLoad(resMan);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
	
		super.postLoad(resMan);
		font = resMan.getAssetMan().get(Constants.SmallFont, BitmapFont.class);
		

		//boomSfx = resMan.getAssetMan().get("sfx/boom.wav", Sound.class);
		
	}

	public void setOrder(OrderType newOrder){
		currentOrder = newOrder;
		this.setAnim(getOrderAnim(currentOrder));
	}
	
	public void setAlive(boolean value){alive = value;}
	
	public static Animation getOrderAnim(OrderType order){
		switch (order){
		case MoveLeft:
			return animleft;
		case MoveUp:
			return animleft;
		default: 
			return animRight;
		
		}
	}

	@Override
	public boolean doTick(float nextTick) {
		//check for new orders
		Order ord = getOwnerMap().getOrder(x, y);
		if (ord!= null){
			setOrder(ord.getOrder());
			ord.deactivate();
		}
		
		/*
		//check if were at the dropoff
		if (getOwnerMap().getTile(x,y) instanceof GrassDropoff && gemCount > 0){
			getOwnerMap().depostGems(this.gemCount);
			gemCount = 0;
		}
		*/
		
		//test movement
		int dX = 0, dY = 0;
		switch (currentOrder){
			case MoveLeft:
				dX = -1;
				break;
			case MoveRight:
				dX = 1;
				break;
			case MoveUp:
				dY = 1;
				break;
			case MoveDown:
				dY = -1;
				break;
		default:
			break;		
		}
		if (getOwnerMap().getTile(x + dX,y + dY).interact(this,x + dX,y + dY, dX, dY, getOwnerMap())){
			doMove(dX, dY,nextTick);
		}
		return alive;
	}

	public void addGem() {

		TileSet.pickupSfx.play(1f);
		gemCount++;
	}

	public void kill() {
		TileSet.crushSfx.play(1f);	
		getOwnerMap().removeDummy(x, y);
		getOwnerMap().reduceBotCount();
		alive = false;
	}

	@Override
	public void draw(SpriteBatch batch) {
		super.draw(batch);
		font.draw(batch,'�' + Integer.toString(gemCount), this.loc.x * (float) Constants.TileSize, this.loc.y * (float) Constants.TileSize);
	}

	public void depositGems(Map map) {
		if (this.gemCount > 0){
			map.depostGems(gemCount);
			gemCount = 0;
	
		}
		
	}

	
	
}
