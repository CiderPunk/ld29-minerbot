package net.ciderpunk.ld29.ents;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public abstract class SimulationEnt extends MineBotEnt {

	int lastX, lastY;
	
	
	public SimulationEnt() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SimulationEnt(GuiElement owner, int x, int y) {
		super(owner, x, y);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void getTextures(TextureAtlas atlas) {
		// TODO Auto-generated method stub		
	}

	public boolean tick(float nextTick){
		
		getOwnerMap().removeDummy(lastX,lastY);
		//reset loc
		this.loc.x = x;
		this.loc.y = y;
		
		return doTick(nextTick);
	}

	public int getX(){return x;}
	public int getY(){return y;}
	
	public abstract boolean doTick(float nextTick);
	
	public void doMove(int dX, int dY, float time){
		lastX = x;
		lastY = y;
		x+=dX; y+= dY;
		getOwnerMap().setDummy(x,y);
		//check previous location's supports
		getOwnerMap().checkSupport(lastX,lastY);
		
		//FIXME: animate transition
	}
	
	
	
}
