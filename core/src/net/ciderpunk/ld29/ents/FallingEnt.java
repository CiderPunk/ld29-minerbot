package net.ciderpunk.ld29.ents;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.tiles.Tile;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

public abstract class FallingEnt extends SimulationEnt {
	
	boolean falling;

	public FallingEnt() {
		super();
		falling = false;
	}

	public FallingEnt(GuiElement owner, int x, int y) {
		super(owner, x, y);
		falling = false;
	}
	
	protected abstract Tile getTile();
	
	
	@Override
	public boolean doTick(float nextTick) {
		Entity target = this.getOwnerMap().getEntityAt(x, y-1);
		if (target instanceof RoBro && falling){
			((RoBro)target).kill();
		}		
		if (this.getOwnerMap().getTile(x, y-1).isEmpty()){
			falling = true;
			this.doMove(0, -1, nextTick);
			return true;
		}
		else{
			falling = false;
			Tile tile = getTile();
			this.getOwnerMap().setTile(x, y, tile);
			//check the newly positioned tile is stable!
			tile.checkSupport(this.getOwnerMap(), x,y);
			return false;
		}	
	}

	
	
	
}
