package net.ciderpunk.ld29.ents;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.Entity2D;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;

public class Order extends Entity2D implements IResourceUser {

	public static Map<OrderType, TextureRegion> frameMap;
	public enum OrderType{
		MoveUp,
		MoveDown,
		MoveLeft, 
		MoveRight,
		Delete,
		None,
	}
	
	
	protected boolean active;
	protected OrderType orderType; 
	
	public Order() {
		super();
	}

	public Order(int x, int y, OrderType type) {
		super(null, x, y);
		orderType = type;
		active = true;
	}


	@Override
	public Boolean update(float dT) {
		return true;
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		frameMap = new HashMap<OrderType, TextureRegion>();
		frameMap.put(OrderType.MoveUp, atlas.findRegion("arrow_up"));
		frameMap.put(OrderType.MoveDown, atlas.findRegion("arrow_down"));
		frameMap.put(OrderType.MoveLeft, atlas.findRegion("arrow_left"));
		frameMap.put(OrderType.MoveRight, atlas.findRegion("arrow_right"));
		frameMap.put(OrderType.Delete, atlas.findRegion("cross"));
		
	}
	
	public static TextureRegion getTexture(OrderType type){
		return (frameMap != null ? frameMap.get(type) : null);
	}

	@Override
	public void draw(SpriteBatch batch) {
		if (active){
			batch.setColor(Constants.transparent);
			batch.draw(getTexture(this.orderType), loc.x * Constants.TileSize, loc.y * Constants.TileSize);
			batch.setColor(Constants.opaque);
		}
	}	
	public boolean isActive(){return active;}
	public void deactivate(){ active= false;}
	public void activate(){ active = true;}
	public OrderType getOrder(){return this.orderType;}
}
