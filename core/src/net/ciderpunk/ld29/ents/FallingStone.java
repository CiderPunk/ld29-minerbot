package net.ciderpunk.ld29.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.tiles.Tile;
import net.ciderpunk.ld29.tiles.TileSet;

public class FallingStone extends FallingEnt {

	static Frame stoneFrame;
	public FallingStone() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FallingStone(GuiElement owner, int x, int y) {
		super(owner, x, y);
		currentFrame = stoneFrame;
	}

	@Override
	public void getTextures(TextureAtlas atlas) {
		stoneFrame = new Frame(atlas.findRegion("stone"), 0, 0);
	}

	@Override
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		super.draw(batch);
	}

	@Override
	protected Tile getTile() {

		TileSet.thudSfx.play(MathUtils.random(0.4f, 0.6f));		
		return TileSet.getTile('s');
	}
	
	
}
