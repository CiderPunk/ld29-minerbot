package net.ciderpunk.ld29.gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.gui.Label;
import net.ciderpunk.gamebase.gui.TextButton;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.gui.ThemedElement;
import net.ciderpunk.gamebase.gui.GuiElement.HorizontalPosition;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

public class SimConsole extends ThemedElement implements IResourceUser  {

	
	protected final SimConsole self = this;
	TextButton pauseButton;
	Label statusText;
	
	public SimConsole(Theme theme) {
		super(400, 64, HorizontalPosition.Mid, VerticalPosition.Bottom, 0, 16, theme);
		this.theme = theme.getTheme("palette");
	}

	public void doDraw(SpriteBatch batch, int x, int y) {
		theme.getPatch().draw(batch, x, y, this.width, this.height);
		super.doDraw(batch, x, y);
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		super.preLoad(resMan);
	}



	@Override
	public void postLoad(ResourceManager resMan) {
		super.postLoad(resMan);

		TextButton resetButton = new TextButton( HorizontalPosition.Right, VerticalPosition.Top, 16, 16, "Reset", ((MineBotGame)this.getRoot()).getTheme() );
		this.addChild(resetButton);
		resetButton.addListener(new IListener(){
			public void doEvent(Object source){ 
				
				((MineBotGame)self.getRoot()).resetMap(false);

	  	}
		});

		//add start and reset buttons
		pauseButton = new TextButton( HorizontalPosition.Right,  VerticalPosition.Top,76, 16, "Pause", ((MineBotGame)this.getRoot()).getTheme() );
		this.addChild(pauseButton);
		pauseButton.addListener(new IListener(){
			public void doEvent(Object source){ 
				if (((MineBotGame)self.getRoot()).isPaused()){
					((MineBotGame)self.getRoot()).resume();
				}
				else{
					((MineBotGame)self.getRoot()).pause(); 
				}
	  	}
		});
		
		statusText = new Label(HorizontalPosition.Left,  VerticalPosition.Top, 16, 8, "Status", ((MineBotGame)this.getRoot()).getTheme() );
		this.addChild(statusText);
	}

	
	public void setStatus(String value){
		statusText.setText(value);
	}

	public void updatePause(boolean paused) {
		pauseButton.setText( paused ? "Pause"  : "Resume" );
	}
	
	
}
