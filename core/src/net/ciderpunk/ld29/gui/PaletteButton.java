package net.ciderpunk.ld29.gui;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.events.MouseEvent;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;
import net.ciderpunk.ld29.ents.Order;
import net.ciderpunk.ld29.ents.Order.OrderType;


public class PaletteButton extends GuiElement implements IResourceUser {

	static TextureRegion hoverBack;
	static TextureRegion activeBack;
	protected Vector2 touchDown;

	boolean hover, active;
	OrderType orderType;
	
	public PaletteButton( HorizontalPosition xAnch,	VerticalPosition yAnch, int xOffset, int yOffset, OrderType order) {
		super(32, 32, xAnch, yAnch, xOffset, yOffset);
		orderType = order;
		touchDown = new Vector2();
	}


	@Override
	public void doDraw(SpriteBatch batch, int x, int y) {
		if (hover){
			batch.draw(hoverBack, x, y);
		}
		if (active){
			batch.draw(activeBack, x, y);	
		}
		batch.draw(Order.getTexture(orderType), x, y);
	}
	
	
	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}
	
	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		hoverBack =  atlas.findRegion("button_hover");
		activeBack =  atlas.findRegion("button_active");
	}


	@Override
	public boolean onMouseMove(MouseEvent e, float xRel, float yRel) {
		// TODO Auto-generated method stub
		return super.onMouseMove(e, xRel, yRel);
	}
	
	@Override
	public boolean onTouch(TouchEvent e, float xRel, float yRel) {
		touchDown.set(xRel, yRel);
		this.hover = true;
		e.target = this;
		return super.onTouch(e, xRel, yRel);
	}


	@Override
	public boolean onRelease(TouchEvent e) {
		if (hover){
			this.active = !this.active;
		}
		((Palette) this.parent).setActiveTool(this);
		hover = false;
		return true;
	}


	@Override
	public boolean onDrag(TouchEvent e) {
		hover = this.contains(touchDown.x + e.offset.x, touchDown.y + e.offset.y);	
		return super.onDrag(e);
	}
	
	public boolean getIsActive(){ 
		return this.active;
	}
	public void setActive(boolean value){
		this.active = value;
	}
	public OrderType getOrderType() {
		return orderType;
	}

}
