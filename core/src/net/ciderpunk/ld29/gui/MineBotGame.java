package net.ciderpunk.ld29.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.gui.Dialogue;
import net.ciderpunk.gamebase.gui.FpsCounter;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;
import net.ciderpunk.ld29.ents.FallingGem;
import net.ciderpunk.ld29.ents.FallingStone;
import net.ciderpunk.ld29.ents.Order;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.ents.SpaceCaptain;
import net.ciderpunk.ld29.map.Map;
import net.ciderpunk.ld29.tiles.TileSet;


public class MineBotGame extends GameScreen {

	TileSet tileSet;
	Map map;
	Theme theme;
	Palette palette;
	SimConsole simCon;
	int mapIndex;
	Dialogue dialogueFail, dialogueAdequate, dialoguePerfect, dialogueWinrar;
		
	public MineBotGame(int w, int h) {
		super(w, h);
		
		mapIndex = 0;
	}


	@Override
	public void startGame() {
		
		dialogueFail = new Dialogue(300, 100, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Yet another failure,\n add it to the list.", "Restart");
		dialogueFail.addListener(new IListener(){
			@Override
			public void doEvent(Object source) {
	  		((Dialogue)source).setVisible(false);
	  		((MineBotGame) self).resetMap(false);
			} 
			});
		dialogueFail.setVisible(false);
		this.addTopChild(dialogueFail);


		dialogueAdequate = new Dialogue(300, 100, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Congratulations,\n You have done the bare minimum required to finish this level.", "Next Level");
		dialogueAdequate.addListener(new IListener(){
			@Override
			public void doEvent(Object source) {
	  		((Dialogue)source).setVisible(false);
	  		((MineBotGame) self).nextMap();
			} 
			});
		dialogueAdequate.setVisible(false);
		this.addTopChild(dialogueAdequate);

		
		dialoguePerfect = new Dialogue(300, 100, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Perfect!\n glorious victory!", "Next Level");
		dialoguePerfect.addListener(new IListener(){
			@Override
			public void doEvent(Object source) {
	  		((Dialogue)source).setVisible(false);
	  		((MineBotGame) self).nextMap();
			} 
			});
		dialoguePerfect.setVisible(false);
		this.addTopChild(dialoguePerfect);
		
		
		dialogueWinrar = new Dialogue(400, 200, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Epic Victory!\nCongratulations on winning everything, sorry there wasn't an awful lot of it, look for more maps, tile types, ENEMIES, multiplayer?!?! and other stuff in the post-compo Android version!\nDeveloped in 48 hours using LibGDX for Ludum Dare 29\nThanks for playing \n@CiderPunk 27/4/14", "Start Again");
		dialogueWinrar.addListener(new IListener(){
			@Override
			public void doEvent(Object source) {
	  		((Dialogue)source).setVisible(false);
	  		((MineBotGame) self).restartGame();
			} 
			});
		dialogueWinrar.setVisible(false);
		this.addTopChild(dialogueWinrar);
		
		
		
		Dialogue intro = new Dialogue(400, 200, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Welcome!\nYour mission is to collect gems and bring them back to the space captain so she can power her spaceship!\n\nPlace order tokens from the palette onto the map, your trusty RoBro mining unit will follow your instructions to the letter, picking up and gems as he finds them, the counter below the RoBro shows how many he is carrying.", "Start");
		intro.addListener(new IListener(){
			@Override
			public void doEvent(Object source) {
				self.removeChild((GuiElement) source);
			} 
			});
		this.addTopChild(intro);
		
		loadNextMap();
	}

	protected void restartGame() {
		mapIndex = 0;
		loadNextMap();
	}


	protected void nextMap() {
		mapIndex++;
		if (mapIndex >= Constants.mapList.length){
			dialogueWinrar.setVisible(true);
		}
		else{
			loadNextMap();
			resetMap(true);
		}
	}


	protected void loadNextMap(){
		map.loadMap(Constants.mapList[mapIndex]);
		
	}
	
	@Override
	protected void think(float dT) {

	}
	
	@Override
	public void preLoad(ResourceManager resMan) {
		theme = Theme.LoadTheme("theme.json");
		resMan.addResourceUser(theme);
		
		IResourceUser[] ents = { new RoBro(), new Order(), new FallingStone(), new FallingGem(), new SpaceCaptain() };
		resMan.addResourceUser(ents);
		
		
		simCon = new SimConsole(theme);
		simCon.setVisible(false);
		addChild(simCon);
		
		palette = new Palette(theme);
		addChild(palette);
		
		addChild(new FpsCounter(HorizontalPosition.Right, VerticalPosition.Top, 16, 16, Constants.SmallFont, false));
		
		resMan.addResourceUser(TileSet.instance);
		map = new Map(this.width,this.height, palette, simCon);
		
		addChild(map);
		resMan.addResourceUser(map);
		//temp loading ents...

		
		super.preLoad(resMan);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		// TODO Auto-generated method stub
		super.postLoad(resMan);

	}


	
	@Override
	public void doDraw(SpriteBatch batch, int x, int y) {
		
		Gdx.graphics.getGL20().glClearColor(Constants.clearColour.r, Constants.clearColour.g, Constants.clearColour.b, Constants.clearColour.a);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		map.drawMap();

		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		super.doDraw(batch, x, y);
		batch.end();
	}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		super.resize(width, height);
		map.resizeView(width, height);
	}

	//gets root theme
	public Theme getTheme(){ return this.theme; }
	
	//resets the map
	public void resetMap(boolean clearOrders){
		this.map.resetMap(clearOrders);
		this.palette.setVisible(true);
		this.simCon.setVisible(false);
	}
	
	//starts the sim
	public void startSimulation(){
		this.map.startSimulation();
		this.palette.setVisible(false);
		this.simCon.setVisible(true);
		simCon.updatePause(true);
	}


	public void pause() {
		map.Pause();
		simCon.updatePause(false);
	}
	
	public boolean isPaused() {
		return map.isPaused();
	}
	
	public void resume(){
		
		map.Resume();
		simCon.updatePause(true);
	}


	public void fail() {
		dialogueFail.setVisible(true);
	}


	public void adequate() {
		this.dialogueAdequate.setVisible(true);
	}


	public void perfect() {
		// TODO Auto-generated method stub
		dialoguePerfect.setVisible(true);
		dialogueAdequate.setVisible(false);

	}
	
}
