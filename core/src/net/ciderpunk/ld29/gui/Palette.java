package net.ciderpunk.ld29.gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.gui.Label;
import net.ciderpunk.gamebase.gui.TextButton;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.gui.ThemedElement;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.ents.Order.OrderType;

public class Palette extends ThemedElement implements IResourceUser {

	protected final Palette self = this;
	PaletteButton activeTool;
	Label mapName;
	
	public Palette(Theme theme) {
		super(400, 64, HorizontalPosition.Mid, VerticalPosition.Bottom, 0, 16, theme);
		this.theme = theme.getTheme("palette");


	}
	
	public void doDraw(SpriteBatch batch, int x, int y) {
		theme.getPatch().draw(batch, x, y, this.width, this.height);
		super.doDraw(batch, x, y);
	}
	
	@Override
	public void preLoad(ResourceManager resMan) {
		
		//add tool buttons
		this.addChild(new PaletteButton(HorizontalPosition.Left, VerticalPosition.Bottom, 16, 8, OrderType.MoveUp));
		this.addChild(new PaletteButton(HorizontalPosition.Left, VerticalPosition.Bottom, 56,8, OrderType.MoveRight));
		this.addChild(new PaletteButton(HorizontalPosition.Left, VerticalPosition.Bottom, 96, 8, OrderType.MoveDown));
		this.addChild(new PaletteButton(HorizontalPosition.Left, VerticalPosition.Bottom, 136, 8, OrderType.MoveLeft));
		this.addChild(new PaletteButton(HorizontalPosition.Left, VerticalPosition.Bottom, 176, 8, OrderType.Delete));
		
		super.preLoad(resMan);
		
		
	}

	@Override
	public void postLoad(ResourceManager resMan) {

		super.postLoad(resMan);

		mapName = new Label(HorizontalPosition.Left, VerticalPosition.Top, 0, 0, "", ((MineBotGame)this.getRoot()).getTheme());
		addChild(mapName);
		
		//add start and reset buttons
		TextButton goButton = new TextButton(HorizontalPosition.Right,  VerticalPosition.Top, 16, 16, "GO!", ((MineBotGame)this.getRoot()).getTheme() );
		goButton.addListener(new IListener(){
			public void doEvent(Object source){ 
	  		((MineBotGame)self.getRoot()).startSimulation(); 
	  	}
		});
		
		
		this.addChild(goButton);
		
		TextButton resetButton = new TextButton(HorizontalPosition.Right,  VerticalPosition.Top, 66, 16, "Clear", ((MineBotGame)this.getRoot()).getTheme());
		resetButton.addListener( new IListener(){
			@Override
			public void doEvent(Object source) {
	  		((MineBotGame)self.getRoot()).resetMap(true);
			}
		});	
		this.addChild(resetButton);

	}
	
	

	@Override
	public boolean onTouch(TouchEvent e, float xRel, float yRel) {
		// TODO Auto-generated method stub
		super.onTouch(e, xRel, yRel);
		return true;
	}
	
	public void setActiveTool(PaletteButton control){
		if (activeTool != null){
			activeTool.setActive(false);
		}
		activeTool = control.getIsActive() ? control : null;
	}
	
	public OrderType getActiveTool() {
		return activeTool != null ? activeTool.getOrderType() : OrderType.None;
	}

	public void setMapName(String name) {
		mapName.setText(name);
	}

	
}
