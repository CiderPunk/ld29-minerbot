package net.ciderpunk.ld29.tiles;

import net.ciderpunk.ld29.ents.FallingEnt;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
public  class Grass extends TextureTile{

	public Grass(TextureAtlas atlas) {
		super(atlas.findRegion("grass"));
	}
	

	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY,	Map map) {
		// TODO Auto-generated method stub
		return true;
	}
	
	
	@Override
	public void checkSupport(Map map, int x, int y) {
		Tile below = map.getTile(x, y-1);
		if (below.isEmpty()){
			//clear grass so it looks less silly
			map.clearTile(x, y);
		}
	}
	
}
