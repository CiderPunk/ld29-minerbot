package net.ciderpunk.ld29.tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.ld29.map.Vect2I;

/**
 * Created by Matthew on 14/05/2014.
 */
public abstract class ActiveTile extends Tile {
  Vect2I coord;

  protected  ActiveTile(){
    coord=  new Vect2I();
  }

  /**
   * This is called as the first step of a click, any dummy tiles are removed
   */
  public abstract void cleanUp();

  /**
   * called after last tick cleanup to determine if were still falling or whatever
   * @return
   */
  public abstract boolean update();

}
