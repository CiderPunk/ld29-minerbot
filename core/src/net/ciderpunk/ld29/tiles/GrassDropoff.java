package net.ciderpunk.ld29.tiles;

import net.ciderpunk.ld29.ents.FallingEnt;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
public  class GrassDropoff extends Grass{

	public GrassDropoff(TextureAtlas atlas) {
		super(atlas);
	}

	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
		target.depositGems(map);
		return true;
	}

	
	
}
