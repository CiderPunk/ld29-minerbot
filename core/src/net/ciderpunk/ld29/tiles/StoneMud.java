package net.ciderpunk.ld29.tiles;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class StoneMud extends Stone {

	public StoneMud(TextureAtlas atlas) {
		super(atlas.findRegion("stonemud"));
	}

}
