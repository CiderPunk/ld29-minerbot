package net.ciderpunk.ld29.tiles;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;


public class TileSet implements IResourceUser {
	
	public static TileSet instance = new TileSet();

	public static Sound[] digSfx;
	public static Sound thudSfx;
	public static Sound crushSfx;
	public static Sound tinkleSfx;
	public static Sound pickupSfx;
	
	protected Map<Character, Tile> tileMap;
	
	
	
	
	
	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
		

		resMan.getAssetMan().load("sfx/pickup.wav", Sound.class);
		resMan.getAssetMan().load("sfx/crush.wav", Sound.class);
		resMan.getAssetMan().load("sfx/tinkle.wav", Sound.class);
		resMan.getAssetMan().load("sfx/thud.wav", Sound.class);
		resMan.getAssetMan().load("sfx/dig1.wav", Sound.class);
		resMan.getAssetMan().load("sfx/dig2.wav", Sound.class);
		resMan.getAssetMan().load("sfx/dig3.wav", Sound.class);
		
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		// TODO Auto-generated method stub
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		tileMap = new HashMap<Character, Tile>();
		tileMap.put('M', new Mud(atlas));
		tileMap.put('S', new StoneMud(atlas));
		tileMap.put('s', new Stone(atlas));
		tileMap.put('G', new GemMud(atlas));
		tileMap.put('g', new Gem(atlas));
		tileMap.put('z', new Grass(atlas));
		tileMap.put('r', new Grass(atlas));
		tileMap.put('R', new Grass(atlas));
		tileMap.put('K', new GrassDropoff(atlas));
		tileMap.put('k', new GrassDropoff(atlas));
		
		digSfx= new Sound[]{  resMan.getAssetMan().get("sfx/dig1.wav", Sound.class),
				resMan.getAssetMan().get("sfx/dig2.wav", Sound.class),
				resMan.getAssetMan().get("sfx/dig3.wav", Sound.class)};
		
	 thudSfx = resMan.getAssetMan().get("sfx/thud.wav", Sound.class);
	 crushSfx = resMan.getAssetMan().get("sfx/crush.wav", Sound.class);
	 tinkleSfx = resMan.getAssetMan().get("sfx/tinkle.wav", Sound.class);
	 pickupSfx = resMan.getAssetMan().get("sfx/pickup.wav", Sound.class);
		
		
	}

	public static Tile getTile(Character chr){
		return instance.tileMap.containsKey(chr) ? instance.tileMap.get(chr) : Empty.instance;
	}
}