package net.ciderpunk.ld29.tiles;

import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Dummy tile positioned in entity position to prevent collisons
 */
public class Dummy extends Tile {

	public static final Dummy instance = new Dummy();

	public void draw(SpriteBatch batch, float x, float y, float moveDist){}
	
	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
		return false;
	}
	
	@Override
	public boolean isEmpty(){return false;}
}
