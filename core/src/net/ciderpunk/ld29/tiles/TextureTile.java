package net.ciderpunk.ld29.tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Matthew on 15/05/2014.
 */
public class TextureTile extends Tile {

  TextureRegion tex;

  public TextureTile(TextureRegion texture) {
    tex = texture;
  }

  public void draw(SpriteBatch batch, float x, float y, float moveDist){
    batch.draw(tex, x, y);
  }
}
