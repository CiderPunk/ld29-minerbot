package net.ciderpunk.ld29.tiles;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GemMud extends Gem {

	public GemMud(TextureAtlas atlas) {
		super(atlas.findRegion("gemmud"));
	}


}
