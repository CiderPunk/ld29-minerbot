package net.ciderpunk.ld29.tiles;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.ents.FallingEnt;
import net.ciderpunk.ld29.ents.FallingGem;
import net.ciderpunk.ld29.ents.FallingStone;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Stone extends Faller {
	
	public Stone(TextureAtlas atlas) {
		super(atlas.findRegion("stone"));
	}

	public Stone(TextureRegion texture) {
		super(texture);
	}

	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
		if (dY == 0){
			if (map.getTile(x+dX,y).isEmpty()){
				//this stone can be pushed
				map.clearTile(x, y);
				FallingStone ent = new FallingStone(map, x, y);
				map.addEntity(ent);
				ent.doMove(dX,dY, map.getNextTick());
				return true;
			}
		}
		return false;
	}

}
