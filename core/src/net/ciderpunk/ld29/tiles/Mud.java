package net.ciderpunk.ld29.tiles;

import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;




public class Mud extends TextureTile {

	public Mud(TextureAtlas atlas) {
		super(atlas.findRegion("mud"));
	}

	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
		map.clearTile(x, y);
	
		TileSet.digSfx[MathUtils.random(TileSet.digSfx.length-1)].play(MathUtils.random(0.4f, 0.6f));		
		
		return true;
	}

	
	
	
}
