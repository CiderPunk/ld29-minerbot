package net.ciderpunk.ld29.tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

/**
 * Created by Matthew on 15/05/2014.
 */
public class MovingTile extends ActiveTile implements Pool.Poolable{


  public enum MoveType{
    Push,
    Slide,
    Fall,
  }

  Tile tile;
  MoveType move;
  int direction;
  Map map;


  public MovingTile(Map ownerMap){
    super();
    map = ownerMap;
    reset();
  }

  @Override
  public void reset() {
    direction = 0;
    tile = null;
  }

  public void init(Tile movedTile, int x, int y, MoveType moveType, int direction){
    move= moveType;
    tile = movedTile;
    this.coord.set(x,y);
    this.direction = direction;
    updateMovement();
  }

  protected void updateMovement(){
    switch (move) {
      case Fall:
        //dummy current loc
        map.setDummy(coord.x,coord.y);
        this.coord.y -= 1;
        break;
      case Push:
        this.coord.x += direction;
        break;
      case Slide:
        map.setDummy(coord.x,coord.y);
        this.coord.x += direction;
        map.setDummy(coord.x, coord.y-1);
        break;
    }
    map.setTile(coord.x,coord.y, this);
  }

  @Override
  public void cleanUp() {
    switch (move) {
      case Fall:
        map.removeTile(coord.x, coord.y + 1);
        break;
      case Push:
        break;
      case Slide:
        map.removeTile(coord.x - direction, coord.y);
        map.removeTile(coord.x, coord.y - 1);
        break;
    }
  }

  @Override
  public boolean update() {
    if (map.getTile(coord.x, coord.y -1).isEmpty()) {
      move = MoveType.Fall;
      updateMovement();
      return true;
    }
    map.setTile(coord.x, coord.y, this);
    return false;
  }

  @Override
  public void draw(SpriteBatch batch, float x, float y, float moveDist) {
    switch(move){
      case Push:
      case Slide:
        tile.draw(batch, x - (moveDist * direction),y, 0f);
       break;
      case Fall:
        tile.draw(batch, x,y + moveDist, 0f);
        break;
    }
  }

  @Override
  public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
    //uif this is a sideway interaction....
    if (dY == 0){
      return tile.interact(target, x,y,dX,dY, map);
    }
    else{
      //crush time!
      return false;
    }

  }
}
