package net.ciderpunk.ld29.tiles;

import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Empty extends Tile {

	public static final Empty instance = new Empty();

	
	public void draw(SpriteBatch batch, float x, float y, float moveDist){
	}
	
	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
		return true;
	}
	
	@Override
	public boolean isEmpty(){return true;}
}
