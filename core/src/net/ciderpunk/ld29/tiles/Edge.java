package net.ciderpunk.ld29.tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

public class Edge extends Tile {

	public static final Edge instance = new Edge();


  @Override
  public void draw(SpriteBatch batch, float x, float y, float moveDist) {

  }

  @Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map) {
		return false;
	}

}
