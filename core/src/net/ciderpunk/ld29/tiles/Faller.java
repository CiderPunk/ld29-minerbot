package net.ciderpunk.ld29.tiles;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.ents.FallingEnt;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import net.ciderpunk.ld29.map.Vect2I;

public abstract class Faller extends TextureTile {

	public Faller(TextureRegion texture) {
		super(texture);
	}

	@Override
	public boolean isBalanceable(){return false;}

  @Override
  public boolean testSlide(Map map, int x, int y) {

  }

  @Override
  public boolean testFall(Map map, int x, int y) {
    return super.testFall(map, x, y);
  }
}
