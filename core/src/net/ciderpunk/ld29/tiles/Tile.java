package net.ciderpunk.ld29.tiles;

import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import net.ciderpunk.ld29.map.Vect2I;

public abstract class Tile{

  public abstract void draw(SpriteBatch batch, float x, float y, float moveDist);
	
	public boolean isEmpty(){return false;}

	public boolean isBalanceable(){return true;}

	public boolean interact(RoBro target, int x, int y, int dX, int dY, Map map){
		return false;
	}

  public boolean testSlide(Map map, int x, int y){ return false; }
  public boolean testFall(Map map, int x, int y){ return false; }
}
