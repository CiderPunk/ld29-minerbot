package net.ciderpunk.ld29.tiles;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.ld29.ents.FallingEnt;
import net.ciderpunk.ld29.ents.FallingGem;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.map.Map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Gem extends Faller {

	public Gem(TextureAtlas atlas) {
		super(atlas.findRegion("gem"));
	}

	public Gem(TextureRegion texture) {
		super(texture);
	}

	@Override
	public boolean interact(RoBro target, int x, int y, int dX, int dY,	Map map) {
		target.addGem();
		map.clearTile(x, y);
		//notify surrounding tiles
		return true;
	}



}
