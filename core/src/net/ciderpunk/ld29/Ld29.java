package net.ciderpunk.ld29;

import net.ciderpunk.ld29.gui.MineBotGame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Ld29 extends ApplicationAdapter {

	MineBotGame	game;
	
	public void create () {
		float density = com.badlogic.gdx.Gdx.graphics.getDensity();
		if (density <= 2f){
		game = new MineBotGame(640,480);
		}
		else{
			game = new MineBotGame(512,384);
				
			
		}
	}
	
	@Override
	public void dispose() {
		game.dispose();
	}

	@Override
	public void render(){		
		game.update( Gdx.graphics.getDeltaTime() );
	}

	@Override
	public void resize(int width, int height) {
		game.resize(width, height);
	}

	@Override
	public void pause() {
		game.pause();
	}

	@Override
	public void resume() {
		
	}
	
	
	
}
