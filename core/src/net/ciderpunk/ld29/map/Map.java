package net.ciderpunk.ld29.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.PooledLinkedList;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.EntityList;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.events.MouseEvent;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.ld29.Constants;
import net.ciderpunk.ld29.ents.FallingEnt;
import net.ciderpunk.ld29.ents.Order;
import net.ciderpunk.ld29.ents.RoBro;
import net.ciderpunk.ld29.ents.Order.OrderType;
import net.ciderpunk.ld29.ents.SimulationEntityList;
import net.ciderpunk.ld29.ents.SpaceCaptain;
import net.ciderpunk.ld29.gui.MineBotGame;
import net.ciderpunk.ld29.gui.Palette;
import net.ciderpunk.ld29.gui.SimConsole;
import net.ciderpunk.ld29.tiles.*;

import java.util.Iterator;

public class Map extends GuiElement implements IResourceUser{

  //linked list of active tiles
  private PooledLinkedList<ActiveTile> activeTiles;
  //pool of movable tile controllers
  private Pool<MovingTile> movingTilePool;
  //list of tiles to test
  private IntArray tileTestList;



	enum MapMode{ Plan, Simulation, Pause }
	
	MapData data;
	Tile[][] tileMap;
	OrthographicCamera camera;
	Viewport view;
	Vector2 BottomLeft;
	Vector2 TopRight;
	SpriteBatch batch;
	ShapeRenderer shaper;
	protected static Vector2 temp = new Vector2();
	SimulationEntityList ents;
	Order[][] orderMap;
	MapMode mode;
	SpaceCaptain spaceCap; 
	
	int screenTileWidth, screenTileHeight;
	int mapHeight, mapWidth;
	
	int horizon;
	Palette palette;
	SimConsole console;

	float nextTick;
	float tickPeriod = 0.25f;
  float moveDist = 0f;

	String statusTemplate;
	int botCount;
	int gemCount;

  final Map self = this;

	public Map(int width, int height,  Palette paletteControl, SimConsole simCon){
		super(width, height, HorizontalPosition.Left, VerticalPosition.Top, 0,0);
		camera = new OrthographicCamera();
		view = new ExtendViewport(width, height, camera);
		batch = new SpriteBatch();
		BottomLeft = new Vector2();
		TopRight = new Vector2();
		shaper = new ShapeRenderer();
		ents = new SimulationEntityList();
		palette = paletteControl;
		console = simCon;

    movingTilePool = new Pool<MovingTile>(){
      @Override
      protected MovingTile newObject() {
        return new MovingTile(self);
      }
    };

    activeTiles = new PooledLinkedList<ActiveTile>(2000);
    tileTestList = new IntArray(false, 5000);
	}
	
	public void loadMap(String file){
		data = MapData.LoadMapData(file);
		mapHeight = data.height;
		mapWidth = data.width;
		horizon = (data.height - data.horizon) * Constants.TileSize;
		tileMap = new Tile[mapWidth][mapHeight];
		resetMap(true);
		palette.setMapName(data.name);
	}

   /*
   * Fetches a new moving tile from the pool
   * @return
   */
  public MovingTile getNewMovingTile(){
    MovingTile tile = movingTilePool.obtain();
    activeTiles.add(tile);
    return tile;
  }

	public MapMode getMode(){return mode;}
	
	public void startSimulation(){mode = MapMode. Simulation;}
	
	@Override
	public void preLoad(ResourceManager resMan) {
	}

	@Override
	public void postLoad(ResourceManager resMan) {
	}

	public void resetMap(boolean clearOrders) {
		this.mode = MapMode.Plan;
		ents.clear();
		if (clearOrders){
		//clear orderMap
			orderMap = new Order[mapWidth][mapHeight];
		}
		gemCount = 0;
		botCount = 0;
		//recreate tile map
		String charMap = data.getTileData();
		for(int y = 0; y < data.height; y++){
			for(int x = 0; x < data.width; x++){
				Character c = charMap.charAt(((data.height - y  -1)*data.width) + x);
				//check for special characters
				switch(c){
					case 'r':
						ents.add(new RoBro(this, x, y, OrderType.MoveLeft));
						botCount++;
						break;
					case 'R':
						ents.add(new RoBro(this, x, y, OrderType.MoveRight));
						botCount++;
						break;
					case 'K':
						spaceCap = new SpaceCaptain(this, x,y);
						ents.add(spaceCap);
						break;
				}
				tileMap[x][y] = TileSet.instance.getTile(c);
				//reactivate disabled orders 
				Order ord = orderMap[x][y];
				if (ord!= null){ 
					ord.activate(); 
				}
			}
		}
		//position camera
		camera.position.set(data.width * Constants.TileSize / 2, (data.height * Constants.TileSize)  - 100, 0);
		cameraAdjust();
		updateStatus();
	}

	protected void updateStatus(){
		console.setStatus(String.format(Constants.statusTemplate, data.needed, data.perfect, this.gemCount));
	}
	
	
	@Override
	protected void doDraw(SpriteBatch batch, int x, int y) {
		//do nothing, alternative draw for this gui monstrosity!
	}
	
	public void drawMap(){
		camera.update();
		shaper.setProjectionMatrix(camera.combined);
		view.unproject(BottomLeft.set(0,Gdx.graphics.getHeight()));
		
		if (TopRight.y > horizon){
			shaper.begin(ShapeType.Filled);
			shaper.setColor(Constants.skyColour);		
			shaper.rect(BottomLeft.x, horizon, TopRight.x - BottomLeft.x, TopRight.y - horizon);
			shaper.end();
		}
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		renderMap(batch);
		batch.end();
	}
	
	
	protected void renderMap(SpriteBatch batch){

		int minY = (int) Math.floor( BottomLeft.y / (float)Constants.TileSize); 
		int minX = (int) Math.floor( BottomLeft.x / (float)Constants.TileSize); 
		int maxY = minY + screenTileHeight > mapHeight ? mapHeight : minY + screenTileHeight;
		int maxX = minX + screenTileWidth > mapWidth ? mapWidth : minX + screenTileWidth;

		for(int y = minY > 0 ? minY : 0; y < maxY ; y++){
			for(int x = minX > 0 ? minX : 0; x < maxX; x++){
				Tile t = tileMap[x][y];
				if (t!= null){
					t.draw(batch, x * Constants.TileSize, y * Constants.TileSize, this.moveDist);
				}
			}
		}
		
		ents.doDraw(batch);
		for(int y = minY > 0 ? minY : 0; y < maxY ; y++){
			for(int x = minX > 0 ? minX : 0; x < maxX; x++){

				Order o = orderMap[x][y];
				if (o!= null){
					o.draw(batch);
				}
			}
		}

	}
	

	public void resizeView(int width, int height) {
		view.update(width, height);
		cameraAdjust(0f,0f,0f);
	}

	@Override
	public boolean onTouch(TouchEvent e, float xRel, float yRel) {
		e.target = this;
		e.startCapture();
		return true;
	}

	protected void cameraAdjust(){
		this.cameraAdjust(0f,0f,0f);
	}
	
	protected void cameraAdjust(float zoom, float x, float y){
		camera.zoom += zoom;
		if (camera.zoom < 0.5f) {camera.zoom = 0.5f;}
		if (camera.zoom > 2f){camera.zoom = 2f;}
		camera.translate(x * camera.zoom  , y * camera.zoom );
		if (camera.position.x < 0 ){ camera.position.x = 0;}
		if (camera.position.y < 0 ){ camera.position.y = 0;}
		if (camera.position.x > mapWidth * Constants.TileSize ){ camera.position.x =  mapWidth * Constants.TileSize ;}
		if (camera.position.y > mapHeight * Constants.TileSize ){ camera.position.y =  mapHeight * Constants.TileSize ;}

		//update tile positions and stuff
		camera.update();
		screenTileWidth = (int) Math.ceil( camera.viewportWidth * camera.zoom / (float) Constants.TileSize)+ 1;
		screenTileHeight = (int) Math.ceil( camera.viewportHeight  * camera.zoom / (float) Constants.TileSize) + 1;
		
		view.unproject(BottomLeft.set(0,Gdx.graphics.getHeight()));
		view.unproject(TopRight.set(Gdx.graphics.getWidth(), 0));
		
	}
	
	@Override
	public boolean onZoom(TouchEvent e, float zoom) {
		float val = camera.zoom * zoom;
		
		cameraAdjust(val - camera.zoom,0,0);
		return true;
	}
	
	@Override
	public boolean onScroll(MouseEvent e) {
		cameraAdjust( 0.1f * ((float) e.scroll),0,0);
		return true;
	}

	
	@Override
	public boolean onMouseMove(MouseEvent e, float xRel, float yRel) {
		e.target = this;
		return true;
	}

	@Override
	public boolean onDrag(TouchEvent e) {
		cameraAdjust(0f, -e.lastMove.x, -e.lastMove.y);
		return true;
	}

	@Override
	public boolean onRelease(TouchEvent e) {
		if (this.mode == MapMode.Plan && e.pointer == 0 && palette.getActiveTool() != OrderType.None && (e.touchTime < Constants.tapMaxTime || e.offset.len2() <  Constants.dragMaxDist)){
			temp.set(e.rawCoord);
			view.unproject(temp);
			//convert to tile coords..
			int x = (int) Math.floor(temp.x / Constants.TileSize);
			int y = (int) Math.floor(temp.y / Constants.TileSize);
			//check bounds
			if (x >= 0 && x < this.mapWidth && y >= 0 && y < this.mapHeight){
				
				this.orderMap[x][y] =  (palette.getActiveTool() == OrderType.Delete ? null : new Order(x,y,palette.getActiveTool()));
			}
		}
		e.endCapture();
		return super.onRelease(e);
	}


	
	@Override
	public void update(float dT) {
		if (mode != MapMode.Pause){
			ents.doUpdate(dT);
			if (mode == MapMode.Simulation){
				nextTick -=dT;
				if (nextTick < 0){
          //avoid having to loop thousands of time if there's been a massive delay
          if (nextTick < -tickPeriod){
            nextTick = tickPeriod;
          }
					while(nextTick < 0){
						nextTick += tickPeriod;
					}
					doTick(nextTick);
				}

        //move dist is the distance tiles should be from their new position!
        this.moveDist = (1f - (nextTick / tickPeriod)) * (float)Constants.TileSize;
			}
		}
		super.update(dT);
	}
	
	protected void doTick(float tickTime){
    ActiveTile tile;
    //iterate through to remove dummy tiles
    activeTiles.iter();
    while((tile = activeTiles.next()) != null){
      tile.cleanUp();
    }
    //iterate again to update dynamic tile positions
    activeTiles.iter();
    while((tile = activeTiles.next()) != null){
      if (!tile.update()){
        activeTiles.remove();
      }
    }

		//entities... (should be phased out really.
    ents.doTick(tickTime);
    Vect2I coord =  Vect2I.temp;
    for (int i = 0; i< tileTestList.size; i++){
      coord.setFromInt(tileTestList.get(i));
      if (getTile(coord.x, coord.y).isEmpty()) {
        getTile(coord.x, coord.y+1).testFall(this, coord.x, coord.y+1);

      }
    }

	}
	

	public SimulationEntityList getEntities(){ return this.ents;}
	
	public Tile getTile(int x, int y){
		return (x >= mapWidth || x < 0 || y >= mapHeight || y < 0 ? Edge.instance : tileMap[x][y] == null ? Empty.instance : tileMap[x][y] );
	}
	
	public void setTile(int x, int y, Tile newTile){
		tileMap[x][y] = newTile;
	}

	public Order getOrder(int x, int y){
		Order order = this.orderMap[x][y];
		return (order!= null && order.isActive() ? order : null);
	}
	
	public void Resume(){
		if (isPaused()){ this.mode= MapMode.Simulation;  }
	}
	
	public void Pause(){ 
		if (this.mode == MapMode.Simulation){ 
			this.mode= MapMode.Pause;
		}
	}
	public boolean isPaused(){return this.mode == MapMode.Pause;}

	//add a new entity to be tracked
	public void addEntity(FallingEnt ent) {
		ents.add(ent);
	}

	public void removeTile(int x, int y) {
		setTile(x,y, Empty.instance);

    //add a single int encoded version of these coords to our test list...
    tileTestList.add(Vect2I.temp.set(x,y).getInt());
	}

	public void setDummy(int x, int y) {
		if (getTile(x, y).isEmpty()){
			setTile(x,y, Dummy.instance);
		}
	}

	public float getNextTick() {
		return this.nextTick;
	}
	
	public Entity getEntityAt(int x, int y){
		return this.ents.findOccupant(x, y);
	}

	public void reduceBotCount() {
		if (--this.botCount == 0){
			this.spaceCap.cry();
			((MineBotGame)this.getRoot()).fail();
		}
	}

	public void depostGems(int depositedGems) {
		this.gemCount += depositedGems;
		this.updateStatus();
		
		if (gemCount >= data.perfect){
			((MineBotGame)this.getRoot()).perfect();
		}
		else if (gemCount >= data.needed){
			((MineBotGame)this.getRoot()).adequate();
		}
	}
	
	
	
	
}
