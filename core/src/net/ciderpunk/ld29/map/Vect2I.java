package net.ciderpunk.ld29.map;

/**
 * Created by Matthew on 17/05/2014.
 * suipersimple integer vector for storing map coords
 */
public class Vect2I {
  private static final int RIGHT = 0xFFFF;
  public int x,y;
  public static final Vect2I temp = new Vect2I();

  public Vect2I(int x, int y){
    this.x= x;
    this.y= y;
  }

  public Vect2I(){ this(0,0);}


  /**
   * packs x and y into a single int
   * @return
   */
  public int getInt(){
    return (x << 16) | (y & RIGHT);
  }

  /**
   * sets x and y with single int
   * @param val
   */
  public void setFromInt(int val){
    x = val >>> 16;
    y = val & RIGHT;
  }


  public Vect2I add( Vect2I val){
    x += val.x;
    y += val.y;
    return this;
  }

  public Vect2I set(int x, int y){
    this.x= x; this.y = y;
    return this;
  }

}
