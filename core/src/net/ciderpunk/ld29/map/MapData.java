package net.ciderpunk.ld29.map;

import net.ciderpunk.gamebase.gui.Theme;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.Viewport;

public class MapData {
		
	public static MapData LoadMapData(String path){
		Json json = new Json();
		FileHandle file = Gdx.files.internal(path);
		String text = file.readString();
		return json.fromJson(MapData.class, text);		
	}
	
	public  String name;
	public int width, height;
	protected String tileData;
	public int horizon;
	public int needed;
	public int perfect;

	//	removes spaces
	public String getTileData(){
		return tileData.replace(" ","").replace("\n","").replace("\r", "").replace("\t", "");
		
	}

}
