package net.ciderpunk.packer;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;


public class Ld29Packer {
	
  public static void main (String[] args) throws Exception {
  	Settings settings = new Settings();
  	//settings.maxWidth = 512;
  	//settings.maxHeight = 512;
    TexturePacker.process(settings, "../atlas/", "../android/assets", "res");
  }
}